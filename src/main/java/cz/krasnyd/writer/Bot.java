package cz.krasnyd.writer;

import org.apache.log4j.Logger;

import cz.krasnyd.writer.arduino.ArduinoWrite;
import cz.krasnyd.writer.processing.InstructionsModel;
import cz.krasnyd.writer.processing.ProcessTextByInstructions;
import cz.krasnyd.writer.utils.MouseUtils;

public class Bot {

	private final static Logger log = Logger.getLogger(Bot.class);
	public void run() {
		MouseUtils.getInstance().focusWindow();

		ZavData zd = new ZavData();
		log.debug("Getting data from ZAV: " + zd);
		InstructionsModel im = new ProcessTextByInstructions(zd).process();
		log.debug("Text processed from instructions: "+im);

		/*
		InstructionsModel im = new InstructionsModel();
		im.setText("ahojklydfjssaf ud dsafhov fhdoav v němž");
		im.setWritingTime(0);*/

		new ArduinoWrite().run(im);
		MouseUtils.getInstance().dalsiUkol();
	}

}
