package cz.krasnyd.writer;

import java.util.Scanner;

import org.apache.log4j.Logger;

public class UserInput {
	private static Scanner sc = new Scanner(System.in, "cp852");
	
	private static Logger log = Logger.getLogger(UserInput.class); 
	
	public static int readInt() {
		int a = sc.nextInt();
		log.debug("User wrote: "+a);
		return a;
	}
	
	public static String readString() {
		String a = sc.next();
		log.debug("User wrote: "+a);
		return a;
	}
}
