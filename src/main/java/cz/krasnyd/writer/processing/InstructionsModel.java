package cz.krasnyd.writer.processing;

public class InstructionsModel {
	private String text;
	private int writingTime;
	private int minKeystrokes;

	public String getText() {
		return text;
	}

	public int getWritingTime() {
		return writingTime;
	}

	@Override
	public String toString() {
		return "InstructionsModel [text=" + text + ", writingTime=" + writingTime + ", minKeystrokes=" + minKeystrokes
				+ "]";
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setWritingTime(int writingTime) {
		this.writingTime = writingTime;
	}

	public int getMinKeystrokes() {
		return minKeystrokes;
	}

	public void setMinKeystrokes(int minKeystrokes) {
		this.minKeystrokes = minKeystrokes;
	}
}
