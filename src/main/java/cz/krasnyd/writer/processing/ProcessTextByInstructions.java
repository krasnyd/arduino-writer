package cz.krasnyd.writer.processing;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import cz.krasnyd.writer.ZavData;
import cz.krasnyd.writer.utils.Sounds;
import cz.krasnyd.writer.utils.Utils;

public class ProcessTextByInstructions {

	private static final Logger log = Logger.getLogger(ProcessTextByInstructions.class);

	int dobaPsani = 0;
    private static String puvodnivystup = "";
    String vystupPodleZadani = "";

    public ZavData zd = null;
    public ProcessTextByInstructions(ZavData zd) {
    	this.zd = zd;
    }

    public InstructionsModel process() {
    	predelej(zd.getText(), zd.getInstructions());
    	InstructionsModel im = new InstructionsModel();
    	im.setWritingTime(dobaPsani);
    	im.setText(vystupPodleZadani);
    	return im;
    }

    public int predelej(String text, String zadani){
        // ZADANI LOWERCASE
        zadani = zadani.toLowerCase();

        // PŘIPRAVIT VSTUPY
        vystupPodleZadani = text;
        puvodnivystup = text;
        int pocetZnaku = vystupPodleZadani.length();

        try{
            // ZAPSAT ZADÁNÍ DO SOUBORU
            FileWriter fw = new FileWriter("logZadani.txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw);
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            out.println(dateFormat.format(date)+ " - " + zadani);
            out.close();

            if (obsahujeJednou(zadani, "bez kontroly času") || obsahujeJednou(zadani, "bez chyby") || obsahujeJednou(zadani, "Střídejte slova")) {
                log.debug("PŘÍPAD 1 (bez kontroly času/bez chyby)");
                if (!obsahujeJednou(zadani, "bez chyby")){
                    vystupPodleZadani = addBugs(vystupPodleZadani, Utils.rndNumber(0,2));
                }
                dobaPsani = 0;
                return 1;
            } else if ((obsahujeJednou(zadani, "minut"))&& obsahujeJednou(zadani, "postup")) {
                log.debug("PŘÍPAD 2 (x minut, x úhozů)");
                try {
                    String howLong = StringUtils.substringBetween(zadani, "e ", " minut");
                    log.debug("howlong: "+howLong);
                    dobaPsani = Integer.parseInt(howLong);
                } catch (NumberFormatException e){
                    try {
                        String howLong = StringUtils.substringBetween(zadani, "text ", " minut");
                        dobaPsani = Integer.parseInt(howLong);
                    } catch (NumberFormatException ex){
                        try {
                            String howLong = StringUtils.substringBetween(zadani, "následující řádku ", " minut");
                            dobaPsani = Integer.parseInt(howLong);
                        } catch (Exception exc){
                            try {
                                String howLong = StringUtils.substringBetween(" " + zadani, " ", " minut opisujte");
                                dobaPsani = Integer.parseInt(howLong);
                            } catch (Exception exce){
                                log.debug("Nečitelný počet minut: ");
                                Scanner sc = new Scanner(System.in);
                                dobaPsani = sc.nextInt();
                            }
                        }
                    }
                }
                String naPostup = StringUtils.substringBetween(zadani, "postup: ", " čistých");
                int minUhozy;
                try {
                    minUhozy = Integer.parseInt(naPostup);
                } catch (NumberFormatException nb){
                    try {
                        naPostup = StringUtils.substringBetween(zadani, "postup ", " čistých");
                        minUhozy = Integer.parseInt(naPostup);
                    } catch (NumberFormatException nbs){
                        naPostup = StringUtils.substringBetween(zadani, "stačí ", " čistých");
                        minUhozy = Integer.parseInt(naPostup);
                    }
                }

                /* TODO
                if (Screenshot.jePapirove()){
                    BufferedReader in = new BufferedReader(new FileReader("opisy.txt"));
                    String line;
                    while((line = in.readLine()) != null){
                        if (line.equals("Cvičení " + + " ")) {
                            log.debug("Cvičení se nahrálo ze souboru");
                            vystupPodleZadani = in.readLine();
                            puvodnivystup = vystupPodleZadani;
                            break;
                        }
                    }
                    in.close();
                }*/

                log.debug("Minimální počet úhozů: "+minUhozy);
                log.debug("Doba psaní: "+dobaPsani);
                if(StringUtils.startsWith(puvodnivystup,"Lenka")){
                    enlargeText(pocetZnaku, minUhozy+80, true);
                    vystupPodleZadani = addBugs(vystupPodleZadani, Utils.rndNumber(6,10));
                } else {
                    enlargeText(pocetZnaku, minUhozy, true);
                }
                return 1;
            } else if (obsahujeJednou(zadani, "pisujte bez chyby")) {
                log.debug("PŘÍPAD 2.5 (Opisujte bez chyby)");
                int chyby = 0;

                String naPostup = StringUtils.substringBetween(zadani, "postup: ", " úhoz");
                int minUhozy = Integer.parseInt(naPostup);
                log.debug("Min uhozy: " + minUhozy);

                vystupPodleZadani = (puvodnivystup + puvodnivystup).substring(0,minUhozy+Utils.rndNumber(20,80));
                for (int i = 1; i <= chyby; i++) {
                    vystupPodleZadani += "a";
                }
                dobaPsani = 0;
                return 1;
            } else if (obsahujeJednou(zadani, "přeruší") && obsahujeJednou(zadani, "vyhodnotí")) {
                log.debug("PŘÍPAD 3 (po x chyb)");
                String kolikchyb = StringUtils.substringBetween(zadani, "po ", ". chyb");
                int chyby = Integer.parseInt(kolikchyb);

                String naPostup = StringUtils.substringBetween(zadani, "postup: ", " úhoz");
                int minUhozy = Integer.parseInt(naPostup);
                log.debug(chyby + "," + minUhozy);

                vystupPodleZadani = puvodnivystup.substring(0,minUhozy+Utils.rndNumber(20,80));
                for (int i = 1; i <= chyby; i++) {
                    vystupPodleZadani += "a";
                }
                dobaPsani = 0;
                return 1;
            } else if (obsahujeJednou(zadani, "opisem odzadu") || obsahujeJednou(zadani, "písmenech odzadu")) {
                log.debug("PŘÍPAD 4 (opis odzadu)");
                vystupPodleZadani = puvodnivystup + "\n" + new StringBuilder(puvodnivystup).reverse().toString();
                if (obsahujeJednou(zadani, "limit") && obsahujeJednou(zadani, "čas")) {
                    try {
                        dobaPsani = Integer.parseInt(StringUtils.substringBetween(zadani, "(", " minut"));;
                    } catch (NumberFormatException nbs){
                        dobaPsani = Integer.parseInt(StringUtils.substringBetween(zadani, "limit ", " minut"));;
                    }
                } else {
                    dobaPsani = 0;
                }
                return 1;
            } else if (obsahujeJednou(zadani, "bez mezer")){
                log.debug("PŘÍPAD 5 (bez mezer)");
                vystupPodleZadani = puvodnivystup.replaceAll(" ", "");
                dobaPsani = 0;
                return 1;
            }
            // NEFUNGUJE
//            if (obsahujeJednou(zadani, "vyzkoušej")){
//                log.debug("PŘÍPAD 6 (nové písmeno)");
//                String[] zadaniLetters = StringUtils.substringsBetween(zadani, "\"", "\"");
//                StringBuilder builder = new StringBuilder();
//                for(String s : zadaniLetters) {
//                    builder.append(s);
//                }
//                String str = builder.toString();
//                vystupPodleZadani = str;
//
//                dobaPsani = 0;
//                return true;
//            }

            else if (obsahujeJednou(zadani, "vždy jedenkrát")){
                log.debug("PŘÍPAD 6 (nové písmeno)");
                return 2;
            } else if (obsahujeJednou(zadani, "ádku opište") || obsahujeJednou(zadani, "pište řádku")) {
                log.debug("PŘÍPAD 7 (opsat řádku x-krát)");
                int kolikr = 0;
                try {
                    String kolikrat = StringUtils.substringBetween(zadani, "opište ", "x");
                    if (! StringUtils.isNumeric(kolikrat)) {
                        kolikrat = StringUtils.substringBetween(zadani, "řádku ", "x");
                    }
                    kolikr = Integer.parseInt(kolikrat);
                } catch (Exception e) {
                    log.debug("Číslo nejspíše psané textem.");
                    String kolikrat = StringUtils.substringBetween(zadani, "opište ", "krát");
                    if (! StringUtils.isNumeric(kolikrat)) {
                        kolikrat = StringUtils.substringBetween(zadani, "řádku ", "krát");
                    }
                    switch(kolikrat) {
                        case "dva":
                            kolikr = 2;
                        case "tři":
                            kolikr = 3;
                        case "čtyři":
                            kolikr = 4;
                        case "pět":
                            kolikr = 5;
                        case "šest":
                            kolikr = 6;
                        case "sedm":
                            kolikr = 7;
                        case "osm":
                            kolikr = 8;
                        case "devět":
                            kolikr = 9;
                        case "deset":
                            kolikr = 10;
                        case "jedenáct":
                            kolikr = 11;
                        case "dvanáct":
                            kolikr = 12;
                        case "třináct":
                            kolikr = 13;
                        case "čtrnáct":
                            kolikr = 14;
                        case "patnáct":
                            kolikr = 15;
                        case "šestnáct":
                            kolikr = 16;
                        default:
                            Sounds.playBell();
                    }
                }
                StringBuilder builder = new StringBuilder();
                for (int i = 1; i <= kolikr; i++){
                    builder.append(puvodnivystup).append("\n");
                }
                vystupPodleZadani = builder.toString();
                dobaPsani = 0;
                return 1;
            }/* TODO else if (StringUtils.countMatches(zadani, "vzduch") >= 1){
                log.debug("PŘÍPAD 8 (opis ve vzduchu)");
                log.debug("Počet znaků: "+pocetZnaku);
                int dobapsani = pocetZnaku * 250;
                double sekundy = dobaPsani/1000.0;
                log.debug("Text se bude psát: "+dobapsani+"ms = "+sekundy+"s");
                ArduinoWrite.vzduch = dobapsani;
                dobaPsani = 0;
                vystupPodleZadani = "";
                return 1;
            } */else if (StringUtils.countMatches(zadani, "kontrolou") == 1 && StringUtils.countMatches(zadani, "minut") == 1){
                log.debug("PŘÍPAD 9 (limit minut)");
                dobaPsani = Integer.parseInt(StringUtils.substringBetween(zadani, "limit ", " minut"));
                log.debug("Doba psaní:");
                return 1;
            } else if (obsahujeJednou(zadani, "Opište s kontrolou času") || obsahujeJednou(zadani, "Opište s limitem času")) {
                log.debug("PŘÍPAD 10 (Opis na čas)");
                vystupPodleZadani = addBugs(vystupPodleZadani, Utils.rndNumber(0,2));
                if (obsahujeJednou(zadani, "Opište s kontrolou času")) {
                    dobaPsani = Integer.parseInt(StringUtils.substringBetween(zadani, "limit ", " minut"));
                } else {
                    dobaPsani = Integer.parseInt(StringUtils.substringBetween(zadani, "času (", " minut"));
                }
                log.debug("Opis po " + dobaPsani + " minut.");
                return 1;
            } else {
                Sounds.playBell();
            }
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private static boolean obsahujeJednou(String sentence, String search){
        int pocet = StringUtils.countMatches(sentence.toLowerCase(), search.toLowerCase());
        return pocet == 1;
    }

    public static String addBugs(String novyObsah, int numberOfBugs){
        char[] znakyHoly = novyObsah.toCharArray();

        for (int i = 1; i <= numberOfBugs; i++) {
            int rndPositon = Utils.rndNumber(0, novyObsah.length() - 1);
            int rndChar = Utils.rndNumber(97, 122);
            char ch = (char) rndChar;
            znakyHoly[rndPositon] = ch;
        }

        return new String(znakyHoly);
    }

    public void enlargeText(int pocetZnaku, int minUhozy, boolean enter){
        if (vystupPodleZadani.length() < minUhozy +100) {
            int charsToEnlarge = minUhozy - pocetZnaku + Utils.rndNumber(35,100);
            StringBuilder vys = new StringBuilder(vystupPodleZadani);
            while (charsToEnlarge > 0){
                if (charsToEnlarge <= pocetZnaku){
                    if (enter) vys.append("\n");
                    String newString = puvodnivystup.substring(0, charsToEnlarge);
                    vys.append(newString);
                    charsToEnlarge = 0;
                } else {
                    if (enter) vys.append("\n");
                    vys.append(puvodnivystup);
                    charsToEnlarge = charsToEnlarge - pocetZnaku;
                }
            }
            vystupPodleZadani = vys.toString();
        } else {
            if (vystupPodleZadani.length()-minUhozy > 100){
                vystupPodleZadani = vystupPodleZadani.substring(0,minUhozy+Utils.rndNumber(50,100));
            }
        }
    }

    /*
    public static boolean jePapirove(){
        int[] papirove = {402,437,468,500,527,554,582,612,647,678,708,739,770,802,821,839,857,875,893,911,929,947,965,975,990,1010,1028,1045,1063,1081,1099,1117,1135,1153,1171,1189};
        String text = screenRightTable();
        String[] cviceni = text.split(" ");
        int prvni = Integer.parseInt(cviceni[0]);
        cisloCviceni = prvni;

        for (int aPapirove : papirove) {
            if (aPapirove == prvni) {
                return true;
            }
        }
        return false;

    }*/
}
