package cz.krasnyd.writer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinDef.LPARAM;
import com.sun.jna.platform.win32.WinDef.LRESULT;
import com.sun.jna.platform.win32.WinDef.WPARAM;
import com.sun.jna.win32.StdCallLibrary;

import cz.krasnyd.writer.utils.Utils;

public class ZavData {
    private static Logger log = Logger.getLogger(ZavData.class);

    private String otherText; // TODO zjistit jestli je potřeba -> smazat ho
	private String instructions;
    private String text;
    private String userName;
    private int nextLesson;
    private int lesson;


    interface ExtendedStdLib extends StdCallLibrary {
        ExtendedStdLib INSTANCE = Native.load("user32", ExtendedStdLib.class);
        int WM_SETTEXT = 0x000c;
        int WM_GETTEXT = 0x000D;

        HWND FindWindowA(String lpClassName, String lpWindowName);

        HWND FindWindowExA(HWND hwndParent, HWND hwndChildAfter, String lpClassName,
                           String lpWindowName);

        LRESULT SendMessageA(HWND paramHWND, int paramInt, WPARAM paramWPARAM, LPARAM paramLPARAM);

        LRESULT SendMessageA(HWND editHwnd, int wmGettext, long l, byte[] lParamStr);

        int GetClassNameA(HWND hWnd, byte[] lpString, int maxCount);
    }

    private void getSelectableText() {
        ExtendedStdLib EXT_INSTANCE = ExtendedStdLib.INSTANCE;

        HWND zavhwnd = User32.INSTANCE.FindWindow(null, "ZAV - 1.0.05");
        if (zavhwnd == null) {
            log.error("ZAV window wasn't found - selectable text");
            return;
        }

        List<String> texts = new ArrayList<>();
        User32.INSTANCE.EnumChildWindows(zavhwnd, (hwnd, pntr) -> {
            char[] textBuffer = new char[512];
            char[] textBuffer2 = new char[512];
            User32.INSTANCE.GetClassName(hwnd, textBuffer, 512);
            User32.INSTANCE.GetWindowText(hwnd, textBuffer2, 512);
            String wText = Native.toString(textBuffer);
            String wText2 = Native.toString(textBuffer2);
            if (wText.contains("RichEdit")) {


                //System.out.println("className: " + wText + " title: " + wText2);

                byte[] lParamStr = new byte[2048];
                LRESULT resultBool = EXT_INSTANCE.SendMessageA(hwnd, ExtendedStdLib.WM_GETTEXT, 2048, lParamStr);

                texts.add(Native.toString(lParamStr, "windows-1250"));
            }
            return true;
        }, null);

        otherText = texts.get(0).replace('\u00A0',' ').trim();
        text = texts.get(1).replace('\u00A0',' ').trim();
        instructions = texts.get(2).replace('\u00A0',' ').trim();
    }

    private void getUnselectableText() {
        try {
            //gets program.exe from inside the JAR file as an input stream
            Runtime rt = Runtime.getRuntime();

            URL textDetector = getClass().getClassLoader().getResource(Constants.TEXT_DETECTOR_FILE_NAME);
            if (textDetector == null) {
                throw new ZavDataException("File text-detector wasn't found. ");
            }
            String url = textDetector.toURI().getPath();
            Process proc = rt.exec(url);

            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(proc.getInputStream()));

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(proc.getErrorStream()));

            String exeOutput = Utils.readBufferedReader(stdInput);
            String exeOutputError = Utils.readBufferedReader(stdError);

            if (exeOutput.isEmpty() && exeOutputError.isEmpty()) {
                throw new ZavDataException("Text-detector returns empty string");
            } else if (!exeOutputError.isEmpty()) {
                log.error(exeOutputError + " - unselectable text"); // error returned by exe program
            } else {
                parseJSON(exeOutput);
            }

        } catch (IOException | URISyntaxException | ZavDataException e) {
            log.error("Error while reading unselectable text: ", e);
        }
    }

    private void parseJSON(String json) {
        JSONObject jsonObject = new JSONObject(json);
        userName = parseStringFromJson(jsonObject.get("mLabelName"));
        lesson = parseIntFromJson(jsonObject.get("mLabelLessonField"));
        nextLesson = parseIntFromJson(jsonObject.get("mLabelNextLessonField"));
    }

    private int parseIntFromJson(Object obj) {
        if (obj == JSONObject.NULL) {
            return 0;
        } else {
            return Integer.parseInt(obj.toString());
        }
    }

    private String parseStringFromJson(Object obj) {
        if (obj == JSONObject.NULL) {
            return null;
        } else {
            return obj.toString();
        }
    }

    /**
     * Run Python program for getting unselectable text and JNA script for getting selectable.
     * These data save to the variables.
     */
    public ZavData() {
        getSelectableText();
        getUnselectableText();
        log.info(this.toString());
    }

    @Override
    public String toString() {
        return "otherText='" + otherText + "\'\n" +
                "instructions='" + instructions + "\'\n" +
                "text='" + text + "\'\n" +
                "userName='" + userName + "\'\n" +
                "nextLesson=" + nextLesson + "\'\n" +
                "lesson=" + lesson;
    }

    public static void main(String[] args) {
        Thread.setDefaultUncaughtExceptionHandler(new Log4jBackstop());
        ZavData z = new ZavData();
    }

    public class ZavDataException extends Exception {
        ZavDataException(String message){
            super(message);
        }
    }

    public String getOtherText() {
		return otherText;
	}

	public String getInstructions() {
		return instructions;
	}

	public String getText() {
		return text;
	}

	public String getUserName() {
		return userName;
	}

	public int getNextLesson() {
		return nextLesson;
	}

	public int getLesson() {
		return lesson;
	}

}