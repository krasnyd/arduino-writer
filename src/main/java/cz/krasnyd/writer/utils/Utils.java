package cz.krasnyd.writer.utils;

import org.apache.log4j.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Random;
import java.util.stream.Collectors;

public class Utils {
    public static Logger log = Logger.getLogger(Utils.class);

    public static String readBufferedReader(BufferedReader br) {
        String fullText = br.lines().collect(Collectors.joining("\n"));
        if(fullText.length() != 0 && fullText.charAt(fullText.length() - 1) == '\n'){
            fullText = fullText.substring(0, fullText.length() - 2);
        }
        return fullText;
    }

    public static void waitSomeTime(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ex) {
            log.error("Error while waiting. ", ex);
        }
    }

    public static void waitForEnter() {
        try {
            System.in.read();
        } catch (IOException e) {
            log.error("Error while waiting for user reaction. ", e);
        }
    }

    /**
     * Generates random number between min and max (inclusive)
     * @param min The lower bound
     * @param max The upper bound (including)
     * @return Random number
     */
    public static int rndNumber(int min, int max) {
        return new Random().nextInt(max + 1 - min) + min;
    }
}
