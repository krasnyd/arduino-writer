package cz.krasnyd.writer.utils;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class Sounds {
    public static void playBell(){
        Media hit = new Media(Sounds.class.getClassLoader().getResource("bell.mp3").toExternalForm());
        MediaPlayer mediaPlayer = new MediaPlayer(hit);
        mediaPlayer.setVolume(0.5);
        mediaPlayer.play();
    }
}
