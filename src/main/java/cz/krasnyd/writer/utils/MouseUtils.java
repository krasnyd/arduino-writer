package cz.krasnyd.writer.utils;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;

import cz.krasnyd.writer.Preferences;

public class MouseUtils {
	public Robot r = null;

	private static MouseUtils instance = null;

	private MouseUtils() {
		try {
			r = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public static MouseUtils getInstance() {
		if (instance == null) {
			instance = new MouseUtils();
		}
		return instance;
	}

	public void focusWindow() {
		click(100 + Preferences.screenX, 1);
		Utils.waitSomeTime(100);
		click(100 + Preferences.screenY, 600);
		Utils.waitSomeTime(100);
	}

	public void dalsiUkol() {
		Utils.waitSomeTime(5000);
		click(100 + Preferences.screenX, 1);
		Utils.waitSomeTime(100);
		click(Preferences.screenX + Preferences.screenWidth - 60, Preferences.screenY + 415);
		Utils.waitSomeTime(100);
		click(Preferences.screenX + Preferences.screenWidth - 60, Preferences.screenY + 515);
		Utils.waitSomeTime(500);
	}

	private void click(int x, int y) {
		r.mouseMove(x, y);
		r.mousePress(InputEvent.BUTTON1_DOWN_MASK);
		r.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
	}
}
