package cz.krasnyd.writer;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Enumeration;

import org.apache.log4j.Logger;

import cz.krasnyd.writer.utils.Utils;
import gnu.io.CommPortIdentifier;

public class Main {
	private static Logger log = Logger.getLogger(Main.class);

    public static void main(String[] args) {
    	log.info("PROGRAM START");;
    	askForPort();
    	askForScreenInfo();

    	/*
    	ZavData zd = new ZavData();
    	log.debug(zd);*/


    	System.out.println("Začít psát? (enter)");
    	Utils.waitForEnter();
    	while(true) {
    		Bot bot = new Bot();
    		bot.run();

    	}

    	//System.exit(0); // because JavaFX in static
    }

    private static void askForScreenInfo() {
    	GraphicsDevice devices[] = GraphicsEnvironment.
                getLocalGraphicsEnvironment().
                getScreenDevices();

    	System.out.println("Byli nalezenty tyto monitory. Na kterém píšete?");
        for(int i = 0; i < devices.length; i++){
            System.out.println("["+i+"]" + devices[i].getDefaultConfiguration().getBounds());
        }
        int monitorIndex;
        do {
	        monitorIndex = UserInput.readInt();
	        Rectangle monitorBounds = devices[monitorIndex].getDefaultConfiguration().getBounds();
	        Preferences.screenX = monitorBounds.x;
	        Preferences.screenY = monitorBounds.y;
	        Preferences.screenHeight = monitorBounds.height;
	        Preferences.screenWidth = monitorBounds.width;
        } while(monitorIndex < 0 || monitorIndex > devices.length);
    }

    private static void askForPort() {
    	System.out.println("Dostupné porty: ");

    	ArrayList<CommPortIdentifier> allPorts = new ArrayList<>();
        Enumeration thePorts = CommPortIdentifier.getPortIdentifiers();
        while (thePorts.hasMoreElements()) {
            CommPortIdentifier com = (CommPortIdentifier) thePorts.nextElement();
            if (com.getPortType() == CommPortIdentifier.PORT_SERIAL) {
            	System.out.print(com.getName());
            	allPorts.add(com);
            }
        }


        while(true) {
	        System.out.println("Zadejte číslo portu: ");
	        String portName = "COM" + UserInput.readInt();

	        CommPortIdentifier chosenPort = null;
	        for(CommPortIdentifier port : allPorts) {
	        	if(port.getName().equals(portName)) {
	        		chosenPort = port;
	        		break;
	        	}
	        }

	        if(chosenPort != null) {
	        	Preferences.portInstance = chosenPort;
	        	break;
	        }

        }
    }

    static {
    	com.sun.javafx.application.PlatformImpl.startup(()->{});
        Thread.setDefaultUncaughtExceptionHandler(new Log4jBackstop());
        log.info("Initialize done");
    }
}
