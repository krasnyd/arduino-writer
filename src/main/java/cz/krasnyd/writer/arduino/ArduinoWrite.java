package cz.krasnyd.writer.arduino;

import java.io.IOException;
import java.io.OutputStream;

import cz.krasnyd.writer.Preferences;
import cz.krasnyd.writer.processing.InstructionsModel;
import cz.krasnyd.writer.utils.Utils;
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

public class ArduinoWrite {

    public ArduinoWrite() {

    }

    public void run(InstructionsModel model) {
    	String vstup = model.getText();
    	int psaniSpeed = model.getWritingTime();

        // Předělání vstupu
        String messageString = setup(vstup);
        System.out.println("messageString: "+messageString);

        int delkaTextu = messageString.length();
        int rychlostMin = Utils.rndNumber(Preferences.minSpeed-20,Preferences.minSpeed+20);
        int rychlostMax = Utils.rndNumber(Preferences.maxSpeed-20,Preferences.maxSpeed+20);

        /* TODO zrychlení
        minimRychlost = minimRychlost - zrychleni;
        maximRychlost = maximRychlost - zrychleni;
		*/

        // URČENÍ RYCHLOSTI PSANÍ - POJISTKA PROTI NESTIHNUTÍ
        if (psaniSpeed != 0){
            psaniSpeed = psaniSpeed * 60; // převod na minuty
            psaniSpeed = psaniSpeed - Utils.rndNumber(5,25); // TODO rezerva
            psaniSpeed = psaniSpeed * 1000;
            int averageZnak = psaniSpeed / delkaTextu;
            if (averageZnak < 180) {
                rychlostMax = averageZnak + Utils.rndNumber(15, 30);
                rychlostMin = averageZnak - Utils.rndNumber(15, 30);
            }
        }

		CommPortIdentifier portId = Preferences.portInstance;
		try {
			SerialPort serialPort = (SerialPort) portId.open("SimpleWriteApp", 2000);
			OutputStream outputStream = serialPort.getOutputStream();
			serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
/* TODO VZDUCH
			if (vzduch != 0) {
				outputStream.write('\n');
				Thread.sleep(vzduch);
				outputStream.write('\n');
				vzduch = 0;
			} else {*/


			byte[] barr = messageString.getBytes("US-ASCII");
			for (int i = 0; i < barr.length; i++) {
				outputStream.write(barr[i]);
				Utils.waitSomeTime(Utils.rndNumber(rychlostMin, rychlostMax));
			}

			outputStream.close();
			serialPort.close();


		} catch (PortInUseException e) {
			System.out.println("port in use");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedCommOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }



    private String setup(String mujText){
    	StringBuilder text = new StringBuilder();

		char[] originalText = mujText.toCharArray();
		for (int i = 0; i < originalText.length; i++) {
			switch (originalText[i]) {
			case 'ě':
				text.append('+');
				text.append('e');
				break;
			case 'š':
				text.append('+');
				text.append('s');
				break;
			case 'č':
				text.append('+');
				text.append('c');
				break;
			case 'ř':
				text.append('+');
				text.append('r');
				break;
			case 'ž':
				text.append('+');
				text.append('y');
				break;
			case 'ý':
				text.append('=');
				text.append('z');
				break;
			case 'á':
				text.append('=');
				text.append('a');
				break;
			case 'í':
				text.append('=');
				text.append('i');
				break;
			case 'é':
				text.append('=');
				text.append('e');
				break;
			case 'ú':
				text.append('=');
				text.append('u');
				break;
			case 'Š':
				text.append('+');
				text.append('S');
				break;
			case 'Č':
				text.append('+');
				text.append('C');
				break;
			case 'Ř':
				text.append('+');
				text.append('R');
				break;
			case 'Ž':
				text.append('+');
				text.append('Y');
				break;
			case 'Ě':
				text.append('+');
				text.append('E');
				break;
			case 'Ý':
				text.append('=');
				text.append('Z');
				break;
			case 'Á':
				text.append('=');
				text.append('A');
				break;
			case 'Í':
				text.append('=');
				text.append('I');
				break;
			case 'É':
				text.append('=');
				text.append('E');
				break;
			case 'Ú':
				text.append('=');
				text.append('U');
				break;
			case '1':
				text.append('!');
				break;
			case '2':
				text.append('@');
				break;
			case '3':
				text.append('#');
				break;
			case '4':
				text.append('$');
				break;
			case '5':
				text.append('%');
				break;
			case '6':
				text.append('^');
				break;
			case '7':
				text.append('&');
				break;
			case '8':
				text.append('*');
				break;
			case '9':
				text.append('(');
				break;
			case '0':
				text.append(')');
				break;
			case 'ů':
				text.append(';');
				break;
			case 'ť':
				text.append('+');
				text.append('t');
				break;
			case 'Ť':
				text.append('+');
				text.append('T');
				break;
			case 'ď':
				text.append('+');
				text.append('d');
				break;
			case 'Ď':
				text.append('+');
				text.append('D');
				break;
			case 'ó':
				text.append('=');
				text.append('o');
				break;
			case 'Ó':
				text.append('=');
				text.append('O');
				break;
			case 'ň':
				text.append('+');
				text.append('n');
				break;
			case 'Ň':
				text.append('+');
				text.append('N');
				break;
			case 'z':
				text.append('y');
				break;
			case 'Z':
				text.append('Y');
				break;
			case 'y':
				text.append('z');
				break;
			case 'Y':
				text.append('Z');
				break;
			case '-':
				text.append('/');
				break;
			case '"':
				text.append(':');
				break;
			case '(':
				text.append('}');
				break;
			case ')':
				text.append(']');
				break;
			case ':':
				text.append('>');
				break;
			case '?':
				text.append('<');
				break;
			case '!':
				text.append('\"');
				break;
			case '§':
				text.append('\'');
				break;
			case '/':
				text.append('{');
				break;
			case '%':
				text.append('_');
				break;
			case 'ä':
				text.append('\\');
				text.append('a');
				break;
			case 'ö':
				text.append('\\');
				text.append('o');
				break;
			case 'ü':
				text.append('\\');
				text.append('u');
				break;
			case 'Ä':
				text.append('\\');
				text.append('A');
				break;
			case 'Ö':
				text.append('\\');
				text.append('O');
				break;
			case 'Ü':
				text.append('\\');
				text.append('U');
				break;
			case '+':
				text.append('1');
				break;
			case '=':
				text.append('-');
				break;

			default:
				text.append(originalText[i]);
				break;
			}
        }
		return text.toString(); // remove no-break whitespaces
    }
}