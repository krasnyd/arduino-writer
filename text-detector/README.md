Install all required libraries:
```
python -m pip install -r requirements.txt
```

Make executable file from Python Script and add it to Java resources folder:
```
pyinstaller --clean --win-private-assemblies --onefile text-detector.py --distpath ..\src\main\resources\
```