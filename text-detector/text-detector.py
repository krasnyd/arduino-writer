import json
import sys

from pywinauto import Application, ElementNotFoundError
from pywinauto.application import ProcessNotFoundError
from pywinauto.timings import Timings

try:
    app = Application(backend="uia").connect(path="zavvyuka.exe")
    window = app.window()
    # window.print_control_identifiers()
except (ElementNotFoundError, ProcessNotFoundError):
    # print(json.dumps({'state': 1, 'error_msg': 'ZAV window was not found'}, sort_keys=False, indent=4))
    sys.exit("ZAV window was not found")
else:
    Timings.Fast()
    indexes = ["mLabelName", "mLabelNextLessonField", "mLabelLessonField"]
    jsonGen = {"state": 0}
    for i in range(len(indexes)):
        name = indexes[i]
        try:
            value = window.child_window(auto_id=name).texts()[0]
            # print(name + ": " + value)
            jsonGen[name] = window.child_window(auto_id=name).texts()[0]
        except ElementNotFoundError:
            jsonGen[name] = None
    print(json.dumps(jsonGen, sort_keys=False, indent=4))
