#include <Keyboard.h>

int led = 13;
void setup() {
  
  Serial.begin(9600);
  Keyboard.begin();
}

void loop() {
  
  int fromSerial = Serial.read();
  Keyboard.press(fromSerial);
  delay(25);
  Keyboard.releaseAll();
}


